class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def redirect_to_cozinhacaipira
    redirect_to '/cozinhacaipira'
  end
  def home
    @video = []
    @locations = []
    @map_buttons = []
    @content_buttons = []
    
    for video in Video.all do
      @video << video
      single_video_locations = []
      single_video_map_buttons = []
      single_video_content_buttons = []
      
      for location in video.locations do
        single_video_locations << location
      end
      
      for button in video.map_button do
        single_video_map_buttons << button
      end
      
      for content in video.content_button
        single_video_content_buttons << content
      end
      
      @locations << single_video_locations
      @map_buttons << single_video_map_buttons
      @content_buttons << single_video_content_buttons
    end

  #gon.locations =  @locations
  #gon.videos =      @video
  #gon.map_buttons = @map_buttons
  #gon.content_buttons = @content_buttons
  end
end


