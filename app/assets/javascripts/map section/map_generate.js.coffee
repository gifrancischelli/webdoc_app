###---------------------------------------------------
   TODO: Highlight current mark on click
   TODO: mark.onClock ~> update thumbnail
---------------------------------------------------###

updateMapMenu = (video_title, synopsis, id, timestamp, url, watch_btn_title) ->
    $('#title').text video_title 
    $('#synopsis').text synopsis
    $('#watch-sub-episode').data 
      video_id: id,
      timestamp: timestamp,
      video_url: url
    $('#info-display *:hidden').fadeIn(650)
    
window.initMap = ->

  casa_da_farinha = {
    title:"Casa da Farinha"
    synopsis: "<p>No norte de Ubatuba, no Núcleo Picinguaba, se encontra a Casa da Farinha. O local é um antigo engenho de milho, cana de açucar e álcool construido no final do séc.19 que foi abandonado.</p>
              <p>Na década de 50 foi aproveitada a roda d'água para movimentar os aviamentos de uma casa de farinha que até hoje é utilizada na produção de farinha de mandioca pela comunidade local.</p>
              <p>O meste quilombola Zé Pedro é um dos que trabalham para manter vivas as tradições do local. Neste  vídeo ele recebe a visita de um amigo da Vila de Pescadores, que o convida para o preparo de um Azul Marinho, a Caldeirada Caiçara.</p>",
    timestamp: 4,
    fade: 10,
    coox: 40,
    cooy: 60,
    lat: -23.352293,
    lng: -44.854045,
    video_id: 0
  }
  
  cozinha = {
    id: 1,
    title: "Caldeirada Caiçara",
    url: "ar_VmlFKY2I",
  }
  
  gon.videos = [cozinha]
  gon.map_buttons = [[casa_da_farinha]]
  
  
  # Generate Map
  sao_paulo_coordinates = lat: -22.373416, lng: -48.412382
  map = new google.maps.Map document.getElementById('map'), 
    zoom: 7,
    center: sao_paulo_coordinates,
    mapTypeId: google.maps.MapTypeId.HYBRID,
    scrollwheel: false
  
  # Responsive resize  
  google.maps.event.addDomListener window, "resize", ->
    center = map.getCenter()
    google.maps.event.trigger map, "resize"
    map.setCenter center
    return
  
  
  generateMarks = (locations_all) ->
    # Creates and stores all marks
    marks = []
    for location in locations_all
      this_video_marks = []
      for point in location
        mark = new google.maps.Marker
          position: lat: point.lat, lng: point.lng
          title: point.title
          map: map
        this_video_marks.push mark
      marks.push this_video_marks
      
   # 'Click' event injects current video data inside #watch button  
    #for video_index in [0 .. gon.videos.length - 1]
    for mark in marks[0]
      mark.addListener 'click', ->
        title = this.title
        video_id = 1 
        video_url = "ar_VmlFKY2I"
        synopsis = casa_da_farinha.synopsis
        point_timestamp = casa_da_farinha.timestamp
        
        updateMapMenu title, synopsis, video_id,
                      point_timestamp, video_url, title
                      
        $("#player-div").attr "data-id", video_id
          
            
  # Used in generateMarks
  pointInfo = (info, mark_title) ->
    for locations in gon.map_buttons
      for point in locations
        if point.title == mark_title
          video_id = point.video_id - 1
          timestamp = point.timestamp
          break
          
    video = gon.videos[video_id]
    if info == "video.synopsis"
      return video.synopsis
    if info == "video.title"
      return video.title
    if info == "video.tags"
      return video.tags
    if info == "video.url"
      return video.url
    if info == "video.id"
      return video_id
    if info == "timestamp"
      return timestamp
    if info == "location.text"
      return point.text
  
  # Generate marks
  generateMarks gon.map_buttons
