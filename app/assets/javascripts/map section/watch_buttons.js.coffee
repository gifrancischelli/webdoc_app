jQuery(document).ready ->
    
  $('#watch-sub-episode').on 'click', ->
    url = $('#watch-sub-episode').data("video_url")
    if url != "none"
      id = $('#watch-sub-episode').data("video_id")
      timestamp = $('#watch-sub-episode').data("timestamp")
      changeVideo url, timestamp
    return
  

    
  changeVideo = (url, timestamp = 0) ->
    clearInterval(playerInterval)
    clearTimeout(playTimeout)
    playTimeout = window.setTimeout (->
      if player.getVideoUrl() != url
        player.loadVideoById(url)
      player.seekTo(timestamp)
      player.playVideo()
      return), 1000
    return
