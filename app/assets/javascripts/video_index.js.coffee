jQuery ->

  cozinha = {
    id: 1,
    title: "Caldeirada Caiçara",
    url: "ar_VmlFKY2I",
  }
  
  episodes = [cozinha]
  
  generateStdMenu = (videos) ->
    # Add missing videos, 3 per row
    placeholders = 5
    generateThumbnail(videos, placeholders)
    return
      
  generateThumbnail = (videos, placeholders) ->
        
    for i in [episodes.length - 1 .. 0]
      video = episodes[i]
      url = video.url
      thumb_url = "http://img.youtube.com/vi/" + url + "/hqdefault.jpg"
      img = $('<img>', src: thumb_url)
      a = $('<a>' + video.title + ' </a>')
      a.attr
        "data-url": url,  
        "href": "#home",
        "id": i
      flex_item = $('<div></div>', class: "flex-item index-btn")
      img.appendTo flex_item
      a.appendTo flex_item
      flex_item.appendTo $('#thumbs')
      
      # onClick -> load video
      $('#' + i).on 'click', ->
        url = $(this).data 'url'
        clearTimeout(playTimeout)
        playTimeout = window.setTimeout (->
          if player.getVideoUrl() != url
            player.loadVideoById(url)
          player.playVideo()
          return), 1000
    
    for i in [1..placeholders]
      h1 = $('<h1> Em breve </h1>')
      thumbnail = $('<div>', class: "thumbnail flex-item")
      h1.appendTo(thumbnail)
      thumbnail.appendTo $('#thumbs')
    return
    
  generateStdMenu(episodes)