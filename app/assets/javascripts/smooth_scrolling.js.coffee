jQuery(document).ready ->
  
  # Map 
  
  # Smooth Scrolling
  $("#main-navbar a, #info-display a, #thumbs a, #player-div a").on 'click', (event) ->  
    event.preventDefault()
    hash = this.hash
    
    $('html, body').animate
      scrollTop: $(hash).offset().top, 900,
      -> window.location.hash = hash   
    return

  
  $("iframe").addClass 'embed-responsive-item'
  
  