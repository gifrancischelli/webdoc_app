/* global gon                 */
/* global YT                  */
/* global onPlayerReady       */
/* global onPlayerStateChange */

var url = "ar_VmlFKY2I"
var player;


function onYouTubeIframeAPIReady() {
  player = new YT.Player('youtube', {
    videoId: url,
    playerVars: {
      fs: false },
    events: {
      'onReady': onPlayerReady,
      'onStateChange':  onPlayerStateChange
    }
  });
}

