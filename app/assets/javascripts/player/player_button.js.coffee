window.onPlayerReady = ->
  # Loads last video id data
  $("#player-div").attr "data-id", 0
  $('#playerModal').delay(1000).modal('toggle') 
  
  casa_da_farinha = {
    title:"Casa da Farinha"
    text: "<p>No norte de Ubatuba, no Núcleo Picinguaba, se encontra a Casa da Farinha. O local é um antigo engenho de milho, cana de açucar e álcool construido no final do séc.19 que foi abandonado.</p>
              <p>Na década de 50 foi aproveitada a roda d'água para movimentar os aviamentos de uma casa de farinha que até hoje é utilizada na produção de farinha de mandioca pela comunidade local.</p>
              <p>O meste quilombola Zé Pedro é um dos que trabalham para manter vivas as tradições do local. Neste  vídeo ele recebe a visita de um amigo da Vila de Pescadores, que o convida para o preparo de um Azul Marinho, a Caldeirada Caiçara.</p>",
    timestamp: 4,
    fade: 10,
    coox: 40,
    cooy: 60,
    lat: -23.352293,
    lng: -44.854045,
    video_id: 0
  }
  
  # Generate marks
  cozinha = {
    id: 1,
    title: "Cozinha Caipira",
    url: "ar_VmlFKY2I",
  }

  receita = {
    id: 2
    title: "Veja a receita do Azul Marinho",
    timestamp: 27,
    fade: 39,
    coox: 40,
    cooy: 15,
    content:   "<div id='flex-body'>
  <div style='flex: 1 35%;'><h4>Ingredientes</h4>
  <ul>
    <li>1 1/2kg de peixe em postas (cavala, garoupa, sargo, pampo  e etc)</li>
    <li>8 bananas bem verdes (Nanica ou Sao Tome)</li>
    <li>3 dentes de alho</li>
    <li>2 tomates</li>
    <li>1 cebola grande</li>
    <li>Coentro miudo</li>
    <li>Salsinha</li>
    <li>Cebolinha</li>
    <li>Sal</li>
    <li>Oleo</li>
    <li>Farinha de mandioca</li>
    <li>Pimenta de cheiro></li>
  </ul></div>
  <div style='flex: 1 65%;'><h4>Como fazer</h4>
  <ol>
    <li>Passe sal no peixe e deixe - o em separado por 1 hora.</li>
    <li>Descasque as bananas e deixe - as de molho em agua fria.</li>
    <li>Depois, em uma panela media, doure a cebola e o alho no oleo quente.</li>
    <li>Junte o tomate cortado em pedacinhos e de uma refogada.</li>
    <li>Coloque as bananas, acrescente agua, e um pouquinho de sal.</li>
    <li>Tampe a panela e deixe cozinhando.</li>
    <li>Quando as bananas estiverem macias, junte o peixe, a pimenta de cheiro, e agua ate cobrir tudo.</li>
    <li>Tampe e deixe cozinhar por aproximadamente 10 a 15 minutos.</li>
    <li>Se necessario, acrescente sal a seu gosto.</li>
    <li>Junte, entao, a salsinha, a cebolinha e o coentro picados.</li>
    <li>Deixe cozinhar por mais 2 ou 3 minutos.</li>
    <li>Em seguida, retire o peixe e um pouco de caldo e coloque - os em uma travessa para ser servido, juntamente com 4 das bananas inteiras cozidas.</li>
    <li>Preparo Pirao: Coloque as 4 bananas restantes na panela e amasse - as numa travessa.</li>
    <li>Em seguida, junte - as novamente ao caldo e va adicionando farinha de mandioca aos pouquinhos, mexendo sempre para nao encaroçar.</li>
    <li>Sirva tudo com arroz branco.</li>
    <li>Rendimento: 5 porçoes.</li>
  </ol></div>
  </div>"
  }
  
  origem = {
    title: "A origem do nome Azul Marinho",
    timestamp: 225,
    fade: 310,
    coox: 10,
    cooy: 60,
    content: "<p>Típica iguaria praiana, elevada a patrimônio histórico e cultural de Ubatuba 
    em 2004, o azul-marinho é um dos pratos mais tradicionais da cultura caiçara 
    do Litoral Norte de São Paulo. O nome vem da coloração azulada que ele ganha a 
    partir do cozimento da banana verde com o peixe na panela de ferro.</p>
    
    <p>Nesse processo, o tanino – substância que é responsável pela adstringência, 
    ou sabor de “amarrar a boca”, sentido ao ingerir frutos verdes – desprende-se 
    da banana e libera a cor ao preparado. O peixe e a banana são ingredientes muito 
    frequentes na cozinha caiçara, dada a facilidade de encontrá-los nas regiões litorâneas.</p>
    
    <p>Para que o caldo fique com a cor azul-marinho é fundamental que a receita seja 
    preparada em uma panela de ferro. O prato também tem que ser feito com peixes que 
    dão um bom gosto ao caldo, ideais para fazer refogado: garoupa, sargo, pargo, 
    xaréu, carapau entre outros.</p>
    
    <p>Quando for preparar este tradicional prato, fique atento a variedade da banana 
    verde, que deve ser a nanica. A dica é cozinhar a banana até que ela fique macia 
    antes de acrescentar as postas do peixe na panela.</p>
    
    <p>Fonte: FundArt - Fundação de Arte e Cultura de Ubatuba<p>"
  }
  
  gon.content_buttons = [[receita, origem]]
  gon.videos = [cozinha]
  gon.map_buttons = [[casa_da_farinha]]
  
window.onPlayerStateChange = (event) ->
  # On Play
  if event.data == 1
    
    if !window.playing
        $("#main-navbar").fadeToggle()
        window.playing = true
        
    clearInterval(window.playerInterval)
      
    t = Math.floor(player.getCurrentTime()) + 1
    video_id = $("#player-div").data("id")
    map_buttons = gon.map_buttons[video_id]
    content_buttons = gon.content_buttons[video_id]
        
    window.playerInterval = setInterval (->
      for map_btn in map_buttons
        if t >= map_btn.timestamp && t <= map_btn.fade
          if !document.getElementById("i" + map_btn.timestamp)
            insertMapBtn map_btn
        if t < map_btn.timestamp || t > map_btn.fade
          $("#i" + map_btn.timestamp).remove()
          
      for content_btn in content_buttons
        if t >= content_btn.timestamp && t <= content_btn.fade
          if !document.getElementById("i" + content_btn.timestamp)
            insertContentBtn content_btn
        if t < content_btn.timestamp || t > content_btn.fade
          $("#i" + content_btn.timestamp).remove()
      t += 0.5
      return), 500
  
  # On Pause
  if event.data == 2
    $("#main-navbar").fadeToggle()
    clearInterval(window.playerInterval)
    window.playing = false
  return
  

jQuery(document).ready ->
  
  window.playing = false
  
  $(".navbar a").on 'click', ->
    player.pauseVideo()
    return
  ###
  $("#closeModal").on 'click', ->
    player.playVideo()
    return
  ###  
  
insertMapBtn = (btn) ->
  b = 
    $('<a><span class="glyphicon glyphicon-globe"></span>   ' + btn.title + '</a>').addClass "content-btn"
    .css "left": btn.coox + "%", "top": btn.cooy + "%"
    .attr
      "id": "i" + btn.timestamp,
      "href": "#map-div",
      "data-title": btn.title
    .on 'click', ->
      video = gon.videos[btn.video_id]
      $('#title').text btn.title 
      $('#synopsis').text btn.text
      $('#watch-sub-episode').data 
        video_id: btn.video_id
        timestamp: btn.timestamp
        video_url: "none"
      player.pauseVideo()
      $('#info-display *:hidden').delay(200).fadeIn(650)
      return
    .appendTo('#player-container').fadeIn(300)
  return

insertContentBtn = (btn) ->
  content_btn =
    $('<a> ' + btn.title + '</a>')
    .addClass "content-btn"
    .css "left": btn.coox + "%", "top": btn.cooy + "%"
    .attr 
      "id":   "i" + btn.timestamp,
      "data-toggle": "modal",
      "data-target": "#playerModal"
    .appendTo('#player-container').fadeIn(300)
  content = btn.content
  title = btn.title
  
  content_btn.on 'click', ->
    $('#modalTitle').html title
    $('#modalBody').html content
    player.pauseVideo()
  return