require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase
  test "should get home" do
    get :home
    assert_response :success
    assert_select "title", "Webdoc App Doctela"
  end

  test "should get about" do
    get :about
    assert_response :success
    assert_select "title", "About | Webdoc App Doctela"
  end
end

