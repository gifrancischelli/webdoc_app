class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|
      t.string :title
      t.string :url
      t.string :tags
      t.text :synopsis

      t.timestamps null: false
    end
  end
end
