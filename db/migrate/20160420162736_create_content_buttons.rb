class CreateContentButtons < ActiveRecord::Migration
  def change
    create_table :content_buttons do |t|
      t.string  :title
      t.integer :timestamp
      t.integer :coox
      t.integer :cooy
      t.string  :style
      t.text    :content
      t.integer :fade
      t.references :video, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
