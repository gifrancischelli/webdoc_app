class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :title
      t.float :lat
      t.float :lng
      t.string :x
      t.string :y
      t.integer :timestamp
      t.references :video, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
