class CreateMapButtons < ActiveRecord::Migration
  def change
    create_table :map_buttons do |t|
      t.string  :title
      t.integer :timestamp
      t.integer :coox
      t.integer :cooy
      t.string  :style
      t.text    :text
      t.float   :lat
      t.float   :lng
      f.integer :fade
      t.references :video, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
