# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160428154939) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "content_buttons", force: :cascade do |t|
    t.string   "title"
    t.integer  "timestamp"
    t.integer  "coox"
    t.integer  "cooy"
    t.string   "style"
    t.text     "content"
    t.integer  "video_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "fade"
  end

  add_index "content_buttons", ["video_id"], name: "index_content_buttons_on_video_id", using: :btree

  create_table "locations", force: :cascade do |t|
    t.string   "title"
    t.float    "lat"
    t.float    "lng"
    t.string   "x"
    t.string   "y"
    t.integer  "timestamp"
    t.integer  "video_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "map_buttons", force: :cascade do |t|
    t.string   "title"
    t.integer  "timestamp"
    t.integer  "coox"
    t.integer  "cooy"
    t.string   "style"
    t.string   "reference"
    t.integer  "video_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text     "text"
    t.float    "lat"
    t.float    "lng"
    t.integer  "fade"
  end

  add_index "map_buttons", ["video_id"], name: "index_map_buttons_on_video_id", using: :btree

  create_table "videos", force: :cascade do |t|
    t.string   "title"
    t.string   "url"
    t.string   "tags"
    t.text     "synopsis"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "content_buttons", "videos"
  add_foreign_key "map_buttons", "videos"
end
